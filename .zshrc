#source "${HOME}/.config/zsh/antigen/antigen.zsh"

###########################################################
#                      Plugin stuff
###########################################################
source "${HOME}/.zgen/zgen.zsh"
# check if there's no init script
if ! zgen saved; then
    echo "Creating a zgen save"

    # plugins
    #zgen load chrissicool/zsh-256color

    # completions
    #zgen load zsh-users/zsh-completions src

    #Command suggestions
    #zgen load hchbaw/auto-fu.zsh

    #Git prompt stuff
    zgen load olivierverdier/zsh-git-prompt


    # Send a notification when long running command exits
    zgen load "MichaelAquilina/zsh-auto-notify"

    zgen save
fi
##########################################################

# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000

#Turn on vi mode
bindkey -v

# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/frans/.zshrc'

autoload -Uz compinit
compinit


#Fuzzy command line completion
local FZF_PATH="/usr/share/fzf/completion.zsh"
local FZF_BINDING_PATH="/usr/share/fzf/key-bindings.zsh"
if [ -f ${FZF_PATH} ]; then
    source ${FZF_PATH}
    source ${FZF_BINDING_PATH}

    #CD into a directory using fzf
    function cf()
    {

        #Run fzf on the result of find for all folders
        #dir=`find -type d | fzf --tiebreak=length,begin`
        #dir=`find -type d | fzf`
        dir=`bfs -type d 2> /dev/null | fzf`

		if [[ -n $dir ]]; then
        	cd $dir
		fi
    }
    #CD into a directory using fzf
    function ch()
    {

        #Run fzf on the result of find for all folders
        #dir=`find -type d | fzf --tiebreak=length,begin`
        #dir=`find -type d | fzf`
        dir=`cat ~/.file_index | fzf`

		if [[ -n $dir ]]; then
        	cd $dir
		fi
    }

	function vf()
	{
		file=`bfs . | fzf`

		nvim $file
	}

    export FZF_COMPLETION_TRIGGER=''
    bindkey '^T' fzf-completion
    bindkey '^I' $fzf_default_completion
fi


# --files: List files that would be searched but do not search
# --no-ignore: Do not respect .gitignore, etc...
# --hidden: Search hidden files and folders
# --follow: Follow symlinks
# --glob: Additional conditions for search (in this case ignore everything in the .git/ folder)

export FZF_DEFAULT_COMMAND="rg --files --no-ignore --hidden --follow --glob '!.git/*'"



# Replacing commands with alternatives if they are installed
eza_not_installed=$(which eza 2>/dev/null | grep -v "not found" | wc -l)
if [ ${eza_not_installed} -eq 0 ]; then
     alias ls=/bin/ls --color=auto
else
    alias ls=eza
fi

##Completion stuff
####################################################################
zmodload zsh/complist 
autoload -Uz compinit
compinit
zstyle :compinstall filename '${HOME}/.zshrc'


zstyle ':completion:*:pacman:*' force-list always

zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}

zstyle ':completion:*:*:kill:*' menu yes select

zstyle ':completion:*:*:killall:*' menu yes select

#- complete pacman-color the same as pacman
compdef _pacman pacman-color=pacman

#####################################################################

#Loading stuff
autoload -U promptinit
promptinit
autoload -U colors && colors

# Use surperior alternatives for commands if available


#Aliases
#alias ls='ls --color=auto'
alias r='~/.scripts/runAfterMake.sh'
alias :q=exit
alias q=exit
alias rg='rg --colors path:fg:green'
alias f='bfs | rg'
alias g=git
alias m=make
alias mp='make -j$(getconf _NPROCESSORS_ONLN)'
alias l=ls
alias la="ls -la"
alias ll="ls -l"
alias v=vim
alias c=cd
alias mv='mv -i'
alias ca=cargo
alias xa=xargo
alias sw=swim
alias p3=python3
alias p2=python2
alias ip3=ipython3
alias ip2=ipython2
alias ovim='/usr/bin/vim'
alias jpn='jupyter notebook'
alias clip='wl-copy'

#####################################################################
ZSH_THEME_GIT_PROMPT_CACHE=true
autoload -Uz colors && colors

#Colors for use in the prompt
#local NORMAL_COLOR
local PATH_COLOR='%F{5}'
local ARROW_COLOR='%F{1}'
local VI_I_COLOR='%F{2}'
local VI_N_COLOR='%F{1}'

#The look of the VI mode indicator
local VIM_PROMPT="%{${VI_N_COLOR}%}♦"
local VIM_INSERT_PROMPT="%{${VI_I_COLOR}%}♦"

local GIT_PROMPT='%b$(git_super_status)'

local BG_COLOR="%{$reset_color%}"
#If we are SSHd
if [ -z ${SSH_CLIENT+x} ]; then
	#If $SSH_CLIENT is unset
	BG_COLOR="%{$reset_color%}"
else
	BG_COLOR='%{$bg[cyan]%}'
fi


#Fuzzy tab completion
zstyle ':completion:*' matcher-list '' \
  'm:{a-z\-}={A-Z\_}' \
  'r:[^[:alpha:]]||[[:alpha:]]=** r:|=* m:{a-z\-}={A-Z\_}' \
  'r:|?=** m:{a-z\-}={A-Z\_}'

PROMPT_START=$'%{\e]133;P;k=i\a%}'
PROMPT_END=$'%{\e]133;B\a%}'

function set_rps1 () {
    local VENV_PROMPT=""
    if [ "$VIRTUAL_ENV" ]; then
        VENV_PROMPT="[ ]"
    fi

    #Show hostname in the right prompt
    RPS1="${VENV_PROMPT}${GIT_PROMPT}%{$fg[blue]%}%m%{$reset_color%} %*${PROMPT_END}"
}

precmd_functions+=set_rps1
# set_rps1

function updateVim {
    #Styling the VI prompt
    VI_PROMPT="${${KEYMAP/vicmd/$VIM_PROMPT}/(main|viins)/$VIM_INSERT_PROMPT}"

    #Contents of the lefth prompt
    PS1="${PROMPT_START}${BG_COLOR}${PATH_COLOR}%5~ ${VI_PROMPT} ${ARROW_COLOR}➔ %{$reset_color%}%"

    zle reset-prompt
}
#Function that runs every time the vi mode is changed and when a new line is started
function zle-line-init zle-keymap-select 
{
    #Run autosuggestions
    updateVim
}
function zle-keymap-select 
{
    updateVim
}


zle -N zle-line-init
zle -N zle-keymap-select

#####################################################################
# Notification on long running exit

zstyle ':notify:*' error-title "Command failed (in #{time_elapsed} seconds)"
zstyle ':notify:*' success-title "Command finished (in #{time_elapsed} seconds)"


######################################################################
#History stuff
#Prevent duplicate history
setopt HIST_IGNORE_DUPS

# Save the time and how long a command ran
setopt EXTENDED_HISTORY


VIM_PATH='/usr/bin/nvim'

#Aliasing neovim if is installed
if [ -f ${VIM_PATH} ]; then
	alias vim='nvim'
fi


# not just at the end
setopt completeinword

# PATH stuff
export PATH=$PATH:~/.local/bin
export PATH=$PATH:~/.cargo/bin
export PATH=$PATH:~/bin
export PATH=~/bin/oss-cad-suite/bin:$PATH
export PATH=$PATH:~/.nix-profile/bin
#export PATH=$PATH:~/.config/bspwm


#Enable 256 bit colors over ssh
# export TERM=xterm-256color
#export TERM=xterm

export LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/usr/local/lib
######################################################################
#                   RSA key stuff
######################################################################

if [ -n "$DESKTOP_SESSION" ];then
    # eval $(gnome-keyring-daemon --start)
    # export SSH_AUTH_SOCK
    # To set this up, refer to https://wiki.archlinux.org/title/GNOME/Keyring#SSH_keys
    # but also make sure to start the gcr-ssh-agent.service service
    export SSH_AUTH_SOCK=${XDG_RUNTIME_DIR}/gcr/ssh
fi

#######################################################################
#                   Colored manpages
#######################################################################
if command -v bat > /dev/null 2>&1; then
    # export MANPAGER="sh -c 'col -bx | bat -l man -p'"
else
    man() {
        env \
        LESS_TERMCAP_mb=$'\e[01;31m' \
        LESS_TERMCAP_md=$'\e[01;31m' \
        LESS_TERMCAP_me=$'\e[0m' \
        LESS_TERMCAP_se=$'\e[0m' \
        LESS_TERMCAP_so=$'\e[01;44;33m' \
        LESS_TERMCAP_ue=$'\e[0m' \
        LESS_TERMCAP_us=$'\e[01;32m' \
        man "$@"
    }
fi

#####################################################################
#                       NPM stuff
#####################################################################

PATH="$HOME/.node_modules/bin:$PATH"
export npm_config_prefix=~/.node_modules

#####################################################################
#                       Pyenv stuff
#####################################################################

# export PYENV_ROOT="$HOME/.pyenv"
# command -v pyenv >/dev/null || export PATH="$PYENV_ROOT/bin:$PATH"
# eval "$(pyenv init -)"

#####################################################################
#Keybindings
bindkey -sM vicmd '^[' '^G'
bindkey "^?" backward-delete-char
bindkey -sM vicmd ':' '^G'

#Ctrl+k is up
bindkey '^K' up-line-or-history
bindkey '^J' down-line-or-history

#Make home and end work
bindkey '\e[OH' beginning-of-line
bindkey '\e[OF' end-of-line

#Exit insert mode with jk
bindkey -M viins 'jk' vi-cmd-mode

#Use <C-r> to search for commands in history
bindkey "^R" history-incremental-pattern-search-backward
bindkey "^S" history-incremental-pattern-search-forward

# Allow forward search with ctrl-s instead of the useless suspend
stty stop undef


export HELIX_RUNTIME=~/build/helix/runtime
export EDITOR="hx"

export BROWSER=firefox
export _JAVA_AWT_WM_NONREPARENTING=1

if ! zgen saved; then

    # save all to init script
fi
#Syntax highlighting
zgen load zsh-users/zsh-syntax-highlighting

# Remove once this is default
export CARGO_REGISTRIES_CRATES_IO_PROTOCOL=sparse

export XILINXD_LICENSE_FILE="2100@xilinx.license.it.liu.se"


source <(jj util completion zsh)
