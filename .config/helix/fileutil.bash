
function command_in_helix {
  local pane=$1
  wezterm cli send-text --pane-id "${pane}" --no-paste ":"
  wezterm cli send-text --pane-id "${pane}"
  echo -e "\r" | wezterm cli send-text --pane-id "${pane}" --no-paste
}

function open_file {
  local pane=$1
  while read -r REPLY; do
   echo -e "o ${REPLY}" | command_in_helix "${pane}"
  done
}

