#!/bin/bash

source "${HOME}/.config/helix/fileutil.bash"

if [[ -z $2 ]]; then
  echo "The pwd and pane must be specified"
  exit 1
fi

workdir=$1
pane=$2

xplr "${workdir}" \
  | open_file "${pane}" \
  > /dev/null
