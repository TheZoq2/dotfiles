#!/bin/python

import Monitor
import subprocess
import style

# Tint2 only receives 255 characters worth of name data in total, including
# null terminators. We currently have 36 workspaces, so we need to fit
# it into those 255 chars. This leaves ~7 characters to play with. One is used for null
# termination, 2 for <num>_, so we have 4 characters left. We'll strip all dashes from the name,
# then use the leading 2 characters of the display, and 2 of the trailing chars
# The format is `<num>_Mo
def shorten_mointor_name(name):
    replaced = name.replace("-", "")
    if len(replaced) < 4:
        return replaced
    return replaced[0:2] + replaced[-2:]

#Locate screens
screens = Monitor.getMonitorSetup()

desktopsPerScreen = 9

# Reset disconnected monitors
for name in screens.disconnected:
    callList = ["bspc", "monitor", name, "-r"]
    subprocess.call(callList, universal_newlines = True)

# Add desktops to active monitors
for s in screens.active:
    #s.name = s.name.replace("\n", "")
    print(s.name)
    shortened = shorten_mointor_name(s.name)

    desktopList = []
    for i in range(desktopsPerScreen):
        desktopList.append(str(i+1) + "_" + shortened)
        # print(str(i+1) + "_" + s.getName())
        # desktopList.append(str(i+1))

    callList = ["bspc", "monitor", s.name, "-d"] + desktopList
    #Reset the desktops
    subprocess.call(callList, universal_newlines = True)
    print(callList)

#Add padding to the top to make room for  the bar
#subprocess.call(["bspc", "config", "right_padding", str(style.BAR_HEIGHT + style.BAR_PADDING * 2)])
