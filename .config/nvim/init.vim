"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Setting up vundle

set nocompatible
filetype off

let g:python3_host_prog="/usr/bin/python3"

call plug#begin('~/.vim/plugged')

"Vundle
" Plug 'VundleVim/Vundle.vim'

"Language client plugins
Plug 'prabirshrestha/async.vim'

" Bracket auto completion
Plug 'Raimondi/delimitMate'
" Plug 'cohama/lexima.vim'

" Synctex integration
Plug 'peder2tm/sved'

"Alignment
Plug 'junegunn/vim-easy-align'

" Spliting and merging multi-line things
Plug 'FooSoft/vim-argwrap'

Plug 'mbbill/undotree'

Plug 'lambdalisue/suda.vim'

"Table formating
Plug 'godlygeek/tabular'
Plug 'dhruvasagar/vim-table-mode'
" Mappings on surroud things
Plug 'tpope/vim-surround'
" Case coercion
Plug 'tpope/tpope-vim-abolish'
" Repeating plugin commands
Plug 'tpope/vim-repeat'
" Git integration
Plug 'tpope/vim-fugitive'
" Comment operations using `gc`
Plug 'tpope/vim-commentary'
Plug 'APZelos/blamer.nvim'

Plug 'junegunn/goyo.vim'
Plug 'junegunn/vim-emoji'
" Plug 'junegunn/vim-peekaboo'

Plug 'jupyter-vim/jupyter-vim'

" Statusbar
" Plug 'nvim-lualine/lualine.nvim'

" Show bars to highlight indentation
" Plug 'Yggdroot/indentLine'
" Plug 'nathanaelkane/vim-indent-guides'

"Jumping around
Plug 'easymotion/vim-easymotion'
" Incremental f
Plug 'rhysd/clever-f.vim'


"Fuzzy search
Plug 'ctrlpvim/ctrlp.vim'
Plug 'fisadev/vim-ctrlp-cmdpalette'
Plug 'junegunn/fzf.vim'
Plug 'ibhagwan/fzf-lua'


"Shifting function arguments
Plug 'PeterRincker/vim-argumentative'

"Fancy icons
Plug 'ryanoasis/vim-devicons'
Plug 'tiagofumo/vim-nerdtree-syntax-highlight'


"Add :Bdelete to close buffers without messing with window layout
Plug 'moll/vim-bbye'

" Pandoc workflow and syntax
" Plug 'vim-pandoc/vim-pandoc'
" Plug 'vim-pandoc/vim-pandoc-syntax'

Plug 'lambdalisue/suda.vim'


"Language plugins
"Openscad support
Plug 'choffee/openscad.vim'
"Elm support
Plug 'ElmCast/elm-vim'
"Haskell support
Plug 'neovimhaskell/haskell-vim'
"GMPL syntax
Plug 'maelvalais/gmpl.vim'
"Coffeescript support
Plug 'kchmck/vim-coffee-script'
"Slim support
Plug 'slim-template/vim-slim'
"ruby linting
Plug 'vim-ruby/vim-ruby'
"sxkhd highlighting
Plug 'baskerville/vim-sxhkdrc'
" Gnuplot
Plug 'vlaadbrain/gnuplot.vim'
" Markdown
Plug 'tpope/vim-markdown'
" LLVM Table gen
Plug 'antiagainst/vim-tablegen'
" Typst
Plug 'kaarmu/typst.vim'
Plug 'https://gitlab.com/TheZoq2/nvim-typst-extra'
Plug 'chomosuke/typst-preview.nvim'
Plug 'elkowar/yuck.vim'

"Glsl
Plug 'tikhomirov/vim-glsl'
"Python syntax highlight
" Plug 'hdima/python-syntax'
"Scala support
Plug 'derekwyatt/vim-scala'
"TOML
Plug 'cespare/vim-toml'
"Gdscript
Plug 'calviken/vim-gdscript3'

Plug 'imsnif/kdl.vim'

Plug 'heavenshell/vim-pydocstring', { 'do': 'make install', 'for': 'python' }


"github markdown support
" Plug 'git@github.com:jtratner/vim-flavored-markdown'


"Deoplete language support plugins

"Rust
Plug 'rust-lang/rust.vim'
"Python
Plug 'leafgarland/typescript-vim'

" Jump to matching blocks
Plug 'andymass/vim-matchup'

" Plug 'luochen1990/rainbow'
" Plug 'https://gitlab.com/HiPhish/rainbow-delimiters.nvim'

" Treesitter
" Plug 'thezoq2/nvim-treesitter', {'do': ':TSUpdate', 'branch': 'improve_rust'}
Plug 'nvim-treesitter/nvim-treesitter', {'do': ':TSUpdate'}
Plug 'nvim-treesitter/playground'

" Typst queries
Plug 'https://github.com/uben0/tree-sitter-typst.git'

" Plug 'RRethy/nvim-treesitter-textsubjects'
" Plug 'nvim-treesitter/nvim-treesitter-textobjects'

Plug 'rhysd/conflict-marker.vim'

"Colors
"Plug 'atweiden/vim-colors-behelit'
Plug 'TheZoq2/papercolor-theme'
Plug 'TheZoq2/badwolf'
Plug 'TheZoq2/Papyrus'
Plug 'pappasam/papercolor-theme-slim'


" Code snippets
Plug 'https://gitlab.com/TheZoq2/vim-snippets.git'

Plug 'TheZoq2/chk', {'dir': '~/.dcs/checklists/'}
Plug 'https://gitlab.com/spade-lang/spade-vim.git'

Plug 'https://gitlab.com/TheZoq2/wal-vim'

" Fancy new neovim stuff
Plug 'nvim-neo-tree/neo-tree.nvim'
Plug 'nvim-neotest/nvim-nio'

Plug 'L3MON4D3/LuaSnip'

Plug 'luukvbaal/statuscol.nvim'

Plug 'neovim/nvim-lspconfig'

Plug 'williamboman/mason.nvim', { 'do': ':MasonUpdate' }
Plug 'williamboman/mason-lspconfig.nvim'

Plug 'norcalli/nvim-colorizer.lua'

Plug 'stevearc/profile.nvim'

Plug 'ggandor/leap.nvim'

" Completion (cmp plugins)
Plug 'neovim/nvim-lspconfig'
Plug 'hrsh7th/cmp-nvim-lsp'
Plug 'hrsh7th/cmp-buffer'
Plug 'hrsh7th/cmp-path'
Plug 'hrsh7th/cmp-cmdline'
Plug 'hrsh7th/nvim-cmp'
" Plug 'ray-x/lsp_signature.nvim'
Plug 'saadparwaiz1/cmp_luasnip'

Plug 'onsails/lspkind.nvim'
Plug 'j-hui/fidget.nvim', { 'tag': 'legacy' }
Plug 'simrat39/symbols-outline.nvim'
Plug 'nvim-treesitter/nvim-treesitter-context'

Plug 'https://gitlab.com/TheZoq2/nvim-treesitter-hlbool'
Plug 'https://gitlab.com/TheZoq2/nvim-treesitter-rust-extra.git'

Plug 'MunifTanjim/nui.nvim'
Plug 'rcarriga/nvim-notify'

" Plug 'lukas-reineke/indent-blankline.nvim'

Plug 'stevearc/oil.nvim'

Plug 'Darazaki/indent-o-matic'

" Neovim lua lsp stuff
Plug 'folke/neodev.nvim'

" DAP
" Plug 'mfussenegger/nvim-dap'
" Plug 'rcarriga/nvim-dap-ui'

Plug 'gfanto/fzf-lsp.nvim', { 'branch': 'main' }
Plug 'folke/lsp-colors.nvim'

Plug 'barreiroleo/ltex_extra.nvim'

Plug 'nvim-lua/plenary.nvim'
Plug 'nvim-telescope/telescope.nvim'

Plug 'sindrets/diffview.nvim'

Plug 'simrat39/rust-tools.nvim', {'commit': 'b50125d'}

" Used by rust-tools
Plug 'nvim-lua/popup.nvim'

" Very important load bearing plugsin
" #####
Plug 'edluffy/hologram.nvim'
Plug 'giusgad/pets.nvim'
" #####

" "Nvim autoread
" Plug 'TheZoq2/neovim-auto-autoread'

" Colortheme changer
Plug 'TheZoq2/neovim-colortheme-changer'


call plug#end()
filetype plugin indent on
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""'

set showcmd         " Show (partial) command in status line.

set number          " Show line numbers.
set relativenumber


set hlsearch        " When there is a previous search pattern, highlight all
                    " its matches.

set incsearch       " While typing a search command, show immediately where the
                    " so far typed pattern matches.

set backspace=2     " Influences the working of <BS>, <Del>, CTRL-W
                    " and CTRL-U in Insert mode. This is a list of items,
                    " separated by commas. Each item allows a way to backspace
                    " over something.

set autoindent      " Copy indent from current line when starting a new line
                    " (typing <CR> in Insert mode or when using the "o" or "O"
                    " command).

set ruler           " Show the line and column number of the cursor position,
                    " separated by a comma.

set mouse=a         " Enable the use of the mouse.

set hidden          "Allow unsaved buffers to be in the background

set cursorline		"Highlight the current line

set signcolumn=no   " Disable the sigils in to the left of the line numbers which are used to inidcate warnings

" Prevent comments from being inserted on new lines created with `o`
autocmd BufEnter * set formatoptions-=o

"Tab handling and listing
set list
set listchars=tab:¬-,trail:~,extends:>,precedes:<

" Indentation settings
set shiftwidth=4    " Number of spaces to use for each step of (auto)indent.
set tabstop=4       " Number of spaces that a <Tab> in the file counts for.
set expandtab       " Use the appropriate number of spaces to insert a <Tab>.
                    " Spaces are used in indents with the '>' and '<' commands
                    " and when 'autoindent' is on. To insert a real tab when
                    " 'expandtab' is on, use CTRL-V <Tab>.
command Tabs set noexpandtab
command Spacess set expandtab

"Codefolding
set foldmethod=indent
set foldnestmax=2
set foldlevel=0

set inccommand=split

filetype plugin indent on

syntax on

set t_Co=256
"colorscheme atom-dark-256
"colorscheme luna-term
"colorscheme github
" colorscheme PaperColor
colorscheme Papyrus
"colorscheme badwolf
set background=light

"set background=dark
set termguicolors
"let g:quantum_black=1
"colorscheme quantum

"Autoreload files when changed externally
set autoread
if has('nvim')
    "autocmd VimEnter * AutoreadLoop
endif
if has('nvim')
    autocmd VimEnter * StartColorPoll
endif



"Prevent redraw during macros etc.
"set lazyredraw

"Customize tab completion
set wildmode=longest,list,full
set wildmenu

" Fast update time for snappy lsp reference highlights
set updatetime=10


"Highlight lines that are longer than 85 characters
"highlight OverLength ctermbg=darkred ctermfg=white guibg=#FFD9D9
"match OverLength /\%>85v.\+/
set colorcolumn=80

set scrolloff=5

set title
autocmd BufEnter * let &titlestring = expand("%:@")

set noshowmode



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Keybindings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = "\<space>"

map <Leader> <localleader>

map <F4> :tabe
map <F2> :tabp
map <F3> :tabn

"Copy paste from system clipboard
map <C-c> "+y
map <C-p> "+p

"Tab stuff
"map <Leader>l :tabn<Enter>
"map <Leader>h :tabp<Enter>
"map <Leader>e :tabe

"Swedish layout is stupid
map Ö :
map ; :

map <c-d> 10j
map <c-u> 10k
map K k:join<Cr>

"Buffer stuff
map <Leader>j :Buffers<CR>
"map <Leader>j :CtrlPBuffer<CR>
"map <Leader>e :e<Space>
"Close a buffer with space-q
map <Leader>q :Bdelete<CR>
"Show buffer list and prompt for buffer id
:nnoremap <Leader>o :buffers<CR>:buffer<Space>
"Switch to the last buffer
map <Leader>k <C-^>

" Find and replace
map <Leader>r :%s//gc<Left><Left><Left>
vmap <Leader>r :s//gc<Left><Left><Left>
map <Leader>S :nohlsearch<CR>

"map <Leader>g :YcmCompleter GoTo<CR>
map <Leader>g :FzfLua live_grep<CR>

map <Leader>tr :setl rnu!<CR>

map j gj
map k gk

"JK to exit insert mode
imap jk <Esc>
"imap kj <Esc>

" EasyAlign mapings
xmap ga <Plug>(EasyAlign)
nmap ga <Plug>(EasyAlign)

" Undotree binding
map <leader>u :UndotreeToggle<CR>

"Quit when q: is pressed aswell
map q: :q

" Wrap or unwrap arguments
nnoremap <silent> gS :ArgWrap<CR>

map <leader>.e :FZF ~/.config/nvim/<CR>

" Helix-like keybinds
map mm %
map <Leader>y "+y
map <Leader>p "+p




"Reload file when changes happen
au CursorHold * if getcmdwintype() == '' | checktime | endif

"Resize windows when the host window is resized
autocmd VimResized * wincmd =


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                        Delimitmate settings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:delimitMate_expand_cr = 1
let g:delimitMate_balance_matchpairs=1
autocmd FileType verilog let b:delimitMate_quotes = ""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                          Completion tings
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"					Elm stuff
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:elm_setup_keybindings = 0
let g:elm_format_autosave = 0

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Latex stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let g:tex_flavor = "latex"

" Neovim seems to autoload a latex plugin which conceals certain symbols
autocmd FileType tex set conceallevel=0

" Surround word with a tag when <Leader>s is pressed
autocmd BufEnter *.tex nmap <Leader>s ysiw}i\

" Synctex forward search on <Leader>g
autocmd BufEnter *.tex nmap <Leader>dg :call SVED_Sync()<CR>

" Enable spell check and set the language to the only true english
autocmd FileType tex set spell spelllang=en_us
" Do spell checking in files without tex preamble stuff
autocmd FileType tex syntax spell toplevel

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 Latex stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Enable spell check
autocmd FileType markdown set spell spelllang=en_gb

let g:markdown_fenced_languages = ['html', 'python', 'bash=sh', 'rust', 'cpp', 'spade']
let g:markdown_syntax_conceal = 0



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           easymotion stuff
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


"Disable default mapping
let g:EasyMotion_do_mapping = 0

"Make case insensitive
let g:EasyMotion_smartcase = 1

"Activate using space+space
nmap <Leader><Leader> <Plug>(easymotion-overwin-f)

"'Search' for space+g
"nmap <Leader>g <Plug>(easymotion-sn)
"map  <Leader>n <Plug>(easymotion-next)


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Nerdtree config
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"let g:NERDTreeFileExtensionHighlightFullName = 1
"let g:NERDTreeExactMatchHighlightFullName = 1
"let g:NERDTreePatternMatchHighlightFullName = 1

let g:WebDevIconsNerdTreeAfterGlyphPadding = '-'
let g:WebDevIconsUnicodeGlyphDoubleWidth = 0
let g:WebDevIconsUnicodeDecorateFolderNodes = 1
let g:DevIconsEnableFoldersOpenClose = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Linter (ale) config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" \   'rust': ['rust-analyzer'],
let g:ale_linters = {
\   'ghmarkdown': ['mdl'],
\   'haskell': ['hdevtools'],
\   'python': [],
\   'cpp': [],
\   'verilog': ['verilator'],
\   'tcl': ['nagelfar'],
\   'rust': []
\}
"\   'rust': ['rls'],
" 'cpp': ['clangcheck']
"   'haskell': ['stack-ghc-mod', 'hdevtools', 'hlint', 'stack-build', 'stack-ghc'],

let g:ale_cpp_clangcheck_options='-std=c++17 -Wall'

let g:ale_verilog_iverilog_options='-c .verilog_config'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Ctrl P mappings
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"CtrlP stuff
let g:ctrlp_show_hidden = 1


map <Leader>; :CtrlPCmdPalette<Cr>

"ctrlp configuration
"Allow searching through tags with <space>t
" let g:ctrlp_extensions=['tag', 'autoignore']
nnoremap <Leader>T :CtrlPTag<Cr>
"Use current working directory for starting ctrlp
let g:ctrlp_working_path_mode = 'a'

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           FZF config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
let $FZF_DEFAULT_COMMAND = "rg --files --hidden -g '!target' -g '!.git' --no-ignore-vcs"
map <Leader>e :FZF<CR>
map <Leader><C-e> :FZF<CR>

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'Normal'],
  \ 'hl':      ['fg', 'Comment'],
  \ 'fg+':     ['fg', 'CursorLine', 'CursorColumn', 'Normal'],
  \ 'bg+':     ['bg', 'CursorLine', 'CursorColumn'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'PreProc'],
  \ 'border':  ['fg', 'Ignore'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

let g:fzf_layout = { 'window': { 'width': 1, 'height': 0.4, 'yoffset': 1, 'border': 'horizontal' } }
let g:fzf_preview_window = []


function! FzfEditPrompt(file)
    let cmd = ':e ' . a:file . '/'

    call feedkeys(cmd)
endfunction
command! -nargs=1 FzfEditPrompt :call FzfEditPrompt(<f-args>)

map <leader>E :call fzf#run({'source': 'find . -type d -print', 'sink': 'FzfEditPrompt'})<CR>
map <leader>.E :call fzf#run({'source': 'find ~/.dotfiles/.config/nvim -type d -print', 'sink': 'FzfEditPrompt'})<CR>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Nvim completion manager config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" don't give |ins-completion-menu| messages.  For example,
" '-- XXX completion (YYY)', 'match 1 of 2', 'The only match',
set shortmess+=c

" IMPORTANT: :help Ncm2PopupOpen for more information
" For better diagnostic messages with coc
set completeopt=noinsert,menuone,noselect




""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Language server config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" Poor man's lsp context menu
let g:lsp_context_menu_commands = [
            \    'vim.lsp.buf.document_symbol()',
            \    'vim.lsp.buf.declaration()',
            \    'vim.lsp.buf.definition()',
            \    'vim.lsp.buf.implementation()',
            \    'vim.lsp.buf.type_definition()',
            \    'vim.lsp.buf.hover()',
            \    'vim.lsp.buf.references()',
            \    'vim.lsp.buf.formatting()',
            \    'vim.lsp.buf.outgoing_calls()',
            \    'vim.lsp.buf.incoming_calls()',
            \ ]

function Fzf_lsp_context_menu()
    call fzf#run({'sink': 'lua', 'source': g:lsp_context_menu_commands, 'down': 10})
endf

nmap <Leader>lc :call g:Fzf_lsp_context_menu()<cr>


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Clever-f config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let g:clever_f_fix_key_direction = 1
let g:clever_f_across_no_line = 1


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Pandoc config
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""


let g:pandoc#modules#disabled = ["keyboard"]
let g:pandoc#syntax#conceal#use=0

autocmd FileType markdown map <Leader>b :w<Cr>:!pandoc notes.md -o notes.pdf<Cr>

function s:MDSettings()
    if version < 508
      command! -nargs=+ HiLink hi link <args>
    else
      command! -nargs=+ HiLink hi def link <args>
    endif
    " adjust syntax highlighting for LaTeX parts
    "   inline formulas:
    syntax region Statement oneline matchgroup=Delimiter start="\$" end="\$"
    "   environments:
    syntax region Statement matchgroup=Delimiter start="\\begin{.*}" end="\\end{.*}" contains=Statement
    syntax region texBlock matchgroup=Delimiter start="\$\$" end="\$\$" contains=Statement
    "   commands:
    syntax region Statement matchgroup=Delimiter start="{" end="}" contains=Statement
    HiLink texBlock tex
endfunction

" autocmd BufRead,BufNewFile *.md setfiletype markdown
autocmd FileType markdown :call <SID>MDSettings()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       C++ macro expansion
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! ExpandCMacro()
  "get current info
  let l:macro_file_name = "__macroexpand__" . tabpagenr()
  let l:file_row = line(".")
  let l:file_name = expand("%")
  let l:file_window = winnr()
  "create mark
  execute "normal! Oint " . l:macro_file_name . ";"
  execute "w"
  "open tiny window ... check if we have already an open buffer for macro
  if bufwinnr( l:macro_file_name ) != -1
    execute bufwinnr( l:macro_file_name) . "wincmd w"
    setlocal modifiable
    execute "normal! ggdG"
  else
    execute "bot 10split " . l:macro_file_name
    execute "setlocal filetype=cpp"
    execute "setlocal buftype=nofile"
    nnoremap <buffer> q :q!<CR>
  endif
  "read file with gcc
  silent! execute "r!gcc -E " . l:file_name
  "keep specific macro line
  execute "normal! ggV/int " . l:macro_file_name . ";$\<CR>d"
  execute "normal! gg=G"
  "resize window
  execute "normal! G"
  let l:macro_end_row = line(".")
  execute "resize " . l:macro_end_row
  execute "normal! gg"
  "no modifiable
  setlocal nomodifiable
  "return to origin place
  execute l:file_window . "wincmd w"
  execute l:file_row
  execute "normal!u"
  execute "w"
  "highlight origin line
  let @/ = getline('.')
endfunction

autocmd FileType cpp nnoremap <leader>m :call ExpandCMacro()<CR>



""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Query identifiers in limestone
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

function! LimestoneQuery()
    execute "!limestone_explorer query " . expand("<cword>")
endfunction

map <leader>lq :call LimestoneQuery()<cr><esc>

function! FifoQuery()
    execute "!echo '^" . expand("<cword>") . "\\W' > .input_fifogrep"
endfunction

map <leader>f :call FifoQuery()<cr><esc>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Rebase marker highlight
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" disable the default highlight group
let g:conflict_marker_highlight_group = ''

" Include text after begin and end markers
let g:conflict_marker_begin = '^<<<<<<< .*$'
let g:conflict_marker_end   = '^>>>>>>> .*$'

" highlight ConflictMarkerBegin guibg=#2f7366
" highlight ConflictMarkerOurs guibg=#2e5049
" highlight ConflictMarkerTheirs guibg=#344f69
" highlight ConflictMarkerEnd guibg=#2f628e
" highlight ConflictMarkerCommonAncestorsHunk guibg=#754a81


""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                       Rainbow parens
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" let g:rainbow_active = 1
" 
" let g:rainbow_conf = {
" \	'parentheses': ['start=/(/ end=/)/ fold'],
" \	'guifgs': ['firebrick3', 'DarkOrchid3', 'SeaGreen3', 'RoyalBlue3'],
" \	'operators': '',
" \	'separately': {
" \		'*': 0,
" \		'wal': {
" \			'parentheses': ['start=/(/ end=/)/ fold'],
" \		},
" \		'scm': {
" \			'parentheses': ['start=/(/ end=/)/ fold'],
" \		},
" \	}
" \}

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                               Lua configs
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

lua << EOF
require("luasnip.loaders.from_vscode").lazy_load()
require('config/lsp')
require('config/snip')
-- require('config/compe')
require('config/cmp')
require('config/neogit')
require('config/keybinds')
require('config/treesitter')
require"fidget".setup(
    {
        timer = {task_decay = 0}
    }
)
-- require('config/lsp_signature')
-- require('config/dap')
require('config/rust-tools')
require('config/textobjects')
require('config/statuscol')
require('config/symbols_outline')
-- require('config/lualine')
require('config/neo-tree')
require("config/mason")
require('neodev').setup()
require("pets").setup({})
-- indent-blankline
-- require("ibl").setup {
--     -- for example, context is off by default, use this to turn it on
--     scope = {
--         enabled = true
--     }
-- }
require'colorizer'.setup()
-- require("oil").setup()
require("indent-o-matic").setup{}
-- require("config/rainbow-delimiters")
-- require("config/profile")
require'treesitter-context'.setup{
  enable = true, -- Enable this plugin (Can be enabled/disabled later via commands)
}
require('config/leap')
require('config/typst-preview')
EOF

