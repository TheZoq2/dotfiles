require('lualine').setup {
    options = {
        theme="ayu_dark",
    },
    sections = {
        lualine_a = {'mode'},
        lualine_b = {},
        lualine_c = {{'filename', path=1}},
        lualine_x = {'searchcount'},
        lualine_y = {'encoding', 'fileformat', {'filetype', icon_only=true}},
        lualine_z = {'location'}
    },
}
