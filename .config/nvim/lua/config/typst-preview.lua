require 'typst-preview'.setup {
  -- invert_colors = "auto",
  -- Provide the path to binaries for dependencies.
  -- Setting this will skip the download of the binary by the plugin.
  -- Warning: Be aware that your version might be older than the one
  -- required.
  dependencies_bin = {
    -- if you are using tinymist, just set ['typst-preview'] = "tinymist".
    ['typst-preview'] = "tinymist",
    ['websocat'] = "websocat"
  },

  -- This function will be called to determine the root of the typst project
  get_root = function(path_of_main_file)
    return vim.fn.fnamemodify(path_of_main_file, ':p:h')
  end,

  -- This function will be called to determine the main file of the typst
  -- project.
  get_main_file = function(path_of_buffer)
    return path_of_buffer
  end,
}
