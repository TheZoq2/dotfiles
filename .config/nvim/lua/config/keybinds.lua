-- Utility functions for compe and luasnip
local t = function(str)
  return vim.api.nvim_replace_termcodes(str, true, true, true)
end

local check_back_space = function()
  local col = vim.fn.col '.' - 1
  if col == 0 or vim.fn.getline('.'):sub(col, col):match '%s' then
    return true
  else
    return false
  end
end

-- Use (s-)tab to:
--- move to prev/next item in completion menu
--- jump to prev/next snippet's placeholder
local luasnip = require 'luasnip'

_G.tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t '<C-n>'
  elseif luasnip.expand_or_jumpable() then
    return t '<Plug>luasnip-expand-or-jump'
  elseif check_back_space() then
    return t '<Tab>'
  else
    return vim.fn['compe#complete']()
  end
end


_G.s_tab_complete = function()
  if vim.fn.pumvisible() == 1 then
    return t '<C-p>'
  elseif luasnip.jumpable(-1) then
    return t '<Plug>luasnip-jump-prev'
  else
    return t '<S-Tab>'
  end
end

_G.on_ii = function()
  if luasnip.expandable() then
    return t '<Plug>luasnip-expand-snippet'
  else
    return vim.fn['compe#confirm']()
  end
end

_G.jump_next = function()
    return t '<Plug>luasnip-jump-next'
end
_G.jump_prev = function()
    return t '<Plug>luasnip-jump-prev'
end

-- Map tab to the above tab complete functions
vim.api.nvim_set_keymap('i', '<C-n>', 'v:lua.tab_complete()', { expr = true })
vim.api.nvim_set_keymap('s', '<C-n>', 'v:lua.tab_complete()', { expr = true })
vim.api.nvim_set_keymap('i', '<C-p>', 'v:lua.s_tab_complete()', { expr = true })
vim.api.nvim_set_keymap('s', '<C-p>', 'v:lua.s_tab_complete()', { expr = true })

-- Map compe confirm and complete functions
-- vim.api.nvim_set_keymap('i', 'ii', 'v:lua.on_ii()', { expr = true })
vim.api.nvim_set_keymap('i', '<c-space>', 'compe#complete()', { expr = true })

vim.api.nvim_set_keymap('i', '<C-h>', 'v:lua.jump_prev()', { expr = true })
vim.api.nvim_set_keymap('s', '<C-h>', 'v:lua.jump_prev()', { expr = true })
vim.api.nvim_set_keymap('i', '<C-l>', 'v:lua.jump_next()', { expr = true })
vim.api.nvim_set_keymap('s', '<C-l>', 'v:lua.jump_next()', { expr = true })

-- vim.api.nvim_set_keymap('i', '<esc>', '<c-o>:lua Luasnip_current_nodes[vim.api.nvim_get_current_buf()] = nil<cr><esc>', { noremap=true, silent=true })
-- vim.api.nvim_set_keymap('s', '<esc>', '<c-o>:lua Luasnip_current_nodes[vim.api.nvim_get_current_buf()] = nil<cr><esc>', { noremap=true, silent=true })

local function lsp_set_keymap(...) vim.api.nvim_set_keymap(...) end
local lsp_opts =  { noremap=true, silent=true }
-- See `:help vim.lsp.*` for documentation on any of the below functions
lsp_set_keymap('n', '<space>lj', '<cmd>lua vim.lsp.buf.document_symbol()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>lJ', '<cmd>Telescope lsp_workspace_symbols<CR>', lsp_opts)
lsp_set_keymap('n', '<space>lgd', '<cmd>lua vim.lsp.buf.definition()<CR>', lsp_opts)
lsp_set_keymap('n', 'gd', '<cmd>lua vim.lsp.buf.definition()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>i', '<cmd>lua vim.lsp.buf.hover()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>lgi', '<cmd>lua vim.lsp.buf.implementation()<CR>', lsp_opts)
lsp_set_keymap('i', '<C-s>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>lr', '<cmd>lua vim.lsp.buf.rename()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>la', '<cmd>lua vim.lsp.buf.code_action()<CR>', lsp_opts)
lsp_set_keymap('n', '<space>ls', '<cmd>SymbolsOutline<CR>', lsp_opts)
vim.keymap.set('n', '<space>lf', function() vim.lsp.buf.format { async = true } end, lsp_opts)
vim.keymap.set('n', '<space>w', vim.diagnostic.open_float, opts)
vim.keymap.set('n', '[d', function() vim.diagnostic.goto_prev({severity = {min = "ERROR"}}) end)
vim.keymap.set('n', ']d', function() vim.diagnostic.goto_next({severity = {min = "ERROR"}}) end)

local function nmap(...) vim.api.nvim_set_keymap('n', ...) end
-- Debug keymaps
nmap('<space>db', "<cmd>lua require'dap'.toggle_breakpoint()<cr>", {})
nmap('<space>dc', "<cmd>lua require'dap'.continue()<cr>", {})
nmap('<space>ds', "<cmd>lua require'dap'.step_over()<cr>", {})
-- nmap('<space>di', "<cmd>lua require'dap'.step_into()<cr>", {})
nmap('<space>du', "<cmd>lua require'dap'.run_to_cursor()<cr>", {})

nmap('<space>di', "<cmd>lua require('dapui').eval()<cr>", {})

nmap('<space>dd', "<cmd>lua require'dapui'.open()<cr>", {})
nmap('<space>dq', "<cmd>lua require('dapui').close()<cr><cmd>lua require'dap'.disconnect()<cr>", {})

nmap('<leader>n', "<cmd>Neotree toggle<cr>", {})
