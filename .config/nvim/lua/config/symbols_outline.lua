
local opts = {
  auto_close=true,
  position = 'right',
  show_numbers = false,
  show_relative_numbers = false,
  show_symbol_details = true,
  lsp_blacklist = {},
  symbol_blacklist = {'Variable'},
  symbols = {
    File = { icon = "", hl = "CmpItemKindField" },
    Module = { icon = "", hl = "CmpItemKindModule" },
    Namespace = { icon = "", hl = "CmpItemKindNamespace" },
    Package = { icon = "", hl = "CmpItemKindPackage" },
    Class = { icon = "𝓒", hl = "CmpItemKindClass" },
    Method = { icon = "ƒ", hl = "CmpItemKindMethod" },
    Property = { icon = "", hl = "CmpItemKindProperty" },
    Field = { icon = "", hl = "CmpItemKindField" },
    Constructor = { icon = "", hl = "CmpItemKindConstructor" },
    Enum = { icon = "ℰ", hl = "CmpItemKindEnum" },
    Interface = { icon = "ﰮ", hl = "CmpItemKindInterface" },
    Function = { icon = "", hl = "CmpItemKindFunction" },
    Variable = { icon = "", hl = "CmpItemKindVariable" },
    Constant = { icon = "", hl = "CmpItemKindConstant" },
    String = { icon = "𝓐", hl = "CmpItemKindString" },
    Number = { icon = "#", hl = "CmpItemKindNumber" },
    Boolean = { icon = "⊨", hl = "CmpItemKindBoolean" },
    Array = { icon = "", hl = "CmpItemKindArray" },
    Object = { icon = "⦿", hl = "CmpItemKindObject" },
    Key = { icon = "🔐", hl = "CmpItemKindKey" },
    Null = { icon = "NULL", hl = "CmpItemKindNull" },
    EnumMember = { icon = "", hl = "CmpItemKindEnumMember" },
    Struct = { icon = "𝓢", hl = "CmpItemKindStruct" },
    Event = { icon = "🗲", hl = "CmpItemKindEvent" },
    Operator = { icon = "+", hl = "CmpItemKindOperator" },
    TypeParameter = { icon = "𝙏", hl = "CmpItemKindTypeParameter" },
    Component = { icon = "", hl = "CmpItemKindComponent" },
    Fragment = { icon = "", hl = "CmpItemKindFragment" },
  },
}

require("symbols-outline").setup(opts)
