local leap = require('leap')

vim.keymap.set({'n', 'x', 'o'}, '<Leader>h', '<Plug>(leap)')
