local parser_config = require'nvim-treesitter.parsers'.get_parser_configs()
require'nvim-treesitter.install'.prefer_git = true

parser_config.spade = {
  install_info = {
    url = "https://gitlab.com/spade-lang/tree-sitter-spade/", -- local path or git repo
    files = {"src/parser.c"},
    -- optional entries:
    branch = "main", -- default branch in case of git repo if different from master
    generate_requires_npm = false, -- if stand-alone parser without npm dependencies
    requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
  },
  filetype = "spade", -- if filetype does not match the parser name
}

parser_config.wal = {
  install_info = {
    url = "https://gitlab.com/TheZoq2/tree-sitter-wal", -- local path or git repo
    files = {"src/parser.c"},
    -- optional entries:
    branch = "main", -- default branch in case of git repo if different from master
    generate_requires_npm = false, -- if stand-alone parser without npm dependencies
    requires_generate_from_grammar = false, -- if folder contains pre-generated src/parser.c
  },
  filetype = "wal", -- if filetype does not match the parser name
}


parser_config.typst = {
  install_info = {
    url = "https://github.com/uben0/tree-sitter-typst.git", -- local path or git repo
    files = {"src/parser.c", "src/scanner.c"},
  },
  filetype = "typst", -- if filetype does not match the parser name
}

-- require'nvim-treesitter.configs'.setup {
--   highlight = {
--     enable = true,
--     -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
--     -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
--     -- Using this option may slow down your editor, and you may see some duplicate highlights.
--     -- Instead of true it can also be a list of languages
--     --
--     -- This is used to get thingsl like git diff highlighting while TS is on
--     -- additional_vim_regex_highlighting = true,
--   },
-- }

require'nvim-treesitter.configs'.setup {
  highlight = {
    enable = true,

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = false,
  },
}
