if _G.StatusColumn then
  return
end

_G.StatusColumn = {
    handler = {
      fold = function()
        local lnum = vim.fn.getmousepos().line

        -- Only lines with a mark should be clickable
        if vim.fn.foldlevel(lnum) <= vim.fn.foldlevel(lnum - 1) then
          return
        end

        local state
        if vim.fn.foldclosed(lnum) == -1 then
          state = "close"
        else
          state = "open"
        end

        vim.cmd.execute("'" .. lnum .. "fold" .. state .. "'")
      end
    }
}


StatusColumn.set_window = function(value, defer, win)
  vim.defer_fn(function()
    win = win or vim.api.nvim_get_current_win()
    vim.api.nvim_win_set_option(win, "statuscolumn", value)
  end, defer or 1)
end

local function is_virtual_line()
  return vim.v.virtnum < 0
end

local function is_wrapped_line()
  return vim.v.virtnum > 0
end

local function not_in_fold_range()
  return vim.fn.foldlevel(vim.v.lnum) <= 0
end

local function not_fold_start()
  return vim.fn.foldlevel(vim.v.lnum) <= vim.fn.foldlevel(vim.v.lnum - 1)
end

local function fold_opened()
  return vim.fn.foldclosed(vim.v.lnum) == -1
end

local function line_number()
  local lnum = tostring(vim.v.lnum)
  local rnum = vim.v.relnum

  if vim.v.wrap then
    return " " .. string.rep(" ", #lnum)
  end

  if rnum == 0 then
    if #lnum == 1 then
      return "" .. lnum
    else
      return lnum
    end
  else
    return tostring(rnum)
  end
end

local function sign_text()
  local rnum = vim.v.relnum;
  -- Don't draw the sign on the non-relative number
  if rnum == 0 then
    return ""
  else
    local line_text = function(sign)
      -- if sign.name:find("GitSign") then
      return vim.fn.sign_getdefined(sign.name)[1].text
      -- end
      -- return "Constant"
    end

    local buf       = vim.api.nvim_get_current_buf()
    local signs     = vim.fn.sign_getplaced(buf, { group = "*", lnum = vim.v.lnum })[1].signs
    local text = vim.tbl_map(line_text, signs)[1]

    return text or ''
  end
end

local function highlight()
  local line_highlight = function(sign)
    -- if sign.name:find("GitSign") then
    return vim.fn.sign_getdefined(sign.name)[1].texthl
    -- end
    -- return "Constant"
  end

  local buf       = vim.api.nvim_get_current_buf()
  local signs     = vim.fn.sign_getplaced(buf, { group = "*", lnum = vim.v.lnum })[1].signs
  local highlight = vim.tbl_map(line_highlight, signs)

  return highlight or "StatusColumnBorder"
end

local number = function()
  return { "%=", line_number(), " " }
end

-- local fold = function()
--   return { "%#FoldColumn#", "%@v:lua.StatusColumn.handler.fold@", fold_icon() }
-- end

StatusColumn.build = function()
  return table.concat(vim.tbl_flatten({"%#", highlight(), "#",  {"%=", sign_text()}, number() }))
end

-- vim.opt.statuscolumn = [[%!v:lua.StatusColumn.build()]]

