local rt = require("rust-tools")

local extension_path = vim.env.HOME .. '/.vscode-oss/extensions/vadimcn.vscode-lldb-1.8.1/'
local codelldb_path = extension_path .. 'adapter/codelldb'
local liblldb_path = extension_path .. 'lldb/lib/liblldb.so'  -- MacOS: This may be .dylib

rt.setup({
    server = {
        on_attach = function(_, bufnr)
            -- Hover actions
            vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
        end,
    },

    -- Automatically set inlay hints (type hints)
    autoSetHints = false,

    -- These apply to the default RustSetInlayHints command
    tools = {
        inlay_hints = {
            auto=false,
        },
    },

    hover_actions = {
        -- the border that is used for the hover window
        -- see vim.api.nvim_open_win()
        border = "single",
    },

    dap = {
        adapter = require('rust-tools.dap').get_codelldb_adapter(
            codelldb_path, liblldb_path)
    }
})

rt.inlay_hints.disable()
