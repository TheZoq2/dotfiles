
require("neodev").setup({ })

local nvim_lsp = require('lspconfig')

local capabilities = vim.lsp.protocol.make_client_capabilities()
capabilities.textDocument.completion.completionItem.snippetSupport = true

capabilities.textDocument.semanticTokensPorivder = nil


local configs = require 'lspconfig.configs'
local util = require('lspconfig/util')

-- Not quite the right way to do it, but some reddit comment suggested it, 
-- and it works unlike what I think the documenentation says :P
vim.api.nvim_create_autocmd("LspAttach", {
  callback = function(args)
    local client = vim.lsp.get_client_by_id(args.data.client_id)
    client.server_capabilities.semanticTokensProvider = nil
  end,
});

-- Check if the config is already defined (useful when reloading this file)
if not configs.spade_lsp then
 configs.spade_lsp = {
   default_config = {
     cmd = {'spade-language-server'},
     filetypes = {'spade'},
     root_dir = function(fname)
        return util.root_pattern('swim.toml')(fname)
     end,
     settings = {},
   },
 }
end

function LspPin()
vim.lsp.buf.execute_command({ command = 'tinymist.pinMain', arguments = { vim.api.nvim_buf_get_name(0) } })
end


nvim_lsp.rust_analyzer.setup({
    -- on_attach=on_attach,
    capabilities=capabilities,
    settings = {
        ["rust-analyzer"] = {
            capabilities = capabilities,
            flags = {
                debounce_text_changes = 150,
            },
            cargo = {
              loadOutDirsFromCheck = true
            },
            procMacro = {
              enable = true
            },
            experimental = {
                procAttrMacros = true
            }
        }
    }
})

nvim_lsp.ltex.setup({
    on_attach = function(client, bufnr)
        -- your other on_attach functions.
        require("ltex_extra").setup{
            load_langs = { "es-AR", "en-US" }, -- table <string> : languages for witch dictionaries will be loaded
            init_check = true, -- boolean : whether to load dictionaries on startup
            path = nil, -- string : path to store dictionaries. Relative path uses current working directory
            log_level = "none", -- string : "none", "trace", "debug", "info", "warn", "error", "fatal"
        }
    end,
    capabilities=capabilities,
    root_dir = function(filename)
        return util.path.dirname(filename)
    end;
    filetypes = {
        "bib",
        "gitcommit",
        "markdown",
        "org",
        "plaintex",
        "rst",
        "rnoweb",
        "tex",
        "pandoc",
        "typst",
    },
    settings = {
        ltex = {
            checkFrequency="edit",
            language="en-US",
            setenceCacheSize=5000,
            additionalRules = {
                motherTongue= "sv",
                word2VecModel = "/usr/share/word2vec",
                languageModel = "/usr/share/ngrams"
            },
            dictionary = {
                ['en-US'] = { 'pipelining', 'Bluespec', 'SystemVerilog', 'Silice' },
            },
            latex = {
                commands = '{"\texttt": "ignore", "\\documentclass[]{}": "ignore", "\\cite{}": "dummy", "\\cite[]{}": "dummy"}'
            },
            completionEnabled=true,
        },
    }
})

-- Use a loop to conveniently call 'setup' on multiple servers and
-- map buffer local keybindings when the language server attaches
local servers = { 'clangd', 'texlab', 'pyright', 'metals', 'spade_lsp', 'ts_ls', 'lua_ls' }
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
        -- on_attach = on_attach,
        capabilities = capabilities,
        flags = {
            debounce_text_changes = 150,
        }
    }
end

function completion_icons()
        local M = {}

        -- This is kind of an ugly hack to avoid including these characters 
        -- in the fzf search. We just replace them with the math monospace versions
        -- https://qaz.wtf/u/convert.cgi?
        M.icons = {
                File = "𝙵𝚒𝚕𝚎",
                Module = "𝙼𝚘𝚍𝚞𝚕𝚎",
                Namespace = "𝙽𝚊𝚖𝚎𝚜𝚙𝚊𝚌𝚎",
                Package = "𝙿𝚊𝚌𝚔𝚊𝚐𝚎",
                Class = "𝙲𝚕𝚊𝚜𝚜",
                Method = "𝙼𝚎𝚝𝚑𝚘𝚍",
                Property = "𝙿𝚛𝚘𝚙𝚎𝚛𝚝𝚢",
                Field = "𝙵𝚒𝚎𝚕𝚍",
                Constructor = "𝙲𝚘𝚗𝚜𝚝𝚛𝚞𝚌𝚝𝚘𝚛",
                Enum = "𝙴𝚗𝚞𝚖",
                Interface = "𝙸𝚗𝚝𝚎𝚛𝚏𝚊𝚌𝚎",
                Function = "𝙵𝚞𝚗𝚌𝚝𝚒𝚘𝚗",
                Variable = "𝚅𝚊𝚛𝚒𝚊𝚋𝚕𝚎",
                Constant = "𝙲𝚘𝚗𝚜𝚝𝚊𝚗𝚝",
                String = "𝚂𝚝𝚛𝚒𝚗𝚐",
                Number = "𝙽𝚞𝚖𝚋𝚎𝚛",
                Boolean = "𝙱𝚘𝚘𝚕𝚎𝚊𝚗",
                Array = "𝙰𝚛𝚛𝚊𝚢",
                Object = "𝙾𝚋𝚓𝚎𝚌𝚝",
                Key = "𝙺𝚎𝚢",
                Null = "𝙽𝚞𝚕𝚕",
                EnumMember = "𝙴𝚗𝚞𝚖 𝙼𝚎𝚖𝚋𝚎𝚛",
                Struct = "𝚂𝚝𝚛𝚞𝚌𝚝",
                Event = "𝙴𝚟𝚎𝚗𝚝",
                Operator = "𝙾𝚙𝚎𝚛𝚊𝚝𝚘𝚛",
                TypeParameter = "𝚃𝚢𝚙𝚎 𝚙𝚊𝚛𝚊𝚖𝚎𝚝𝚎𝚛"
        }

        function M.setup()
                local kinds = vim.lsp.protocol.SymbolKind
                for i, kind in ipairs(kinds) do
                        kinds[i] = M.icons[kind] or kind
                end
        end

        return M
end

completion_icons().setup()

require'fzf_lsp'.setup()

-- Only show errors+warnings
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
    vim.lsp.diagnostic.on_publish_diagnostics, {
        virtual_text = {
            prefix = '',
            severity = {min = vim.diagnostic.severity.WARN}
        },
    }
)

vim.lsp.set_log_level("warn")


local signs = { Error = " ", Warn = " ", Hint = " ", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end


