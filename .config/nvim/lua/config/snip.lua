-- Luasnip stuff
local ls = require("luasnip")

-- some shorthands...
local s = ls.snippet
local sn = ls.snippet_node
local t = ls.text_node
local i = ls.insert_node
local f = ls.function_node
local c = ls.choice_node
local d = ls.dynamic_node
local l = require("luasnip.extras").lambda
local r = require("luasnip.extras").rep
local p = require("luasnip.extras").partial
local m = require("luasnip.extras").match
local n = require("luasnip.extras").nonempty
local dl = require("luasnip.extras").dynamic_lambda
local types = require("luasnip.util.types")

-- Every unspecified option will be set to the default.
ls.config.set_config({
    history = false,
    -- Update more often, :h events for more info.
    updateevents = "TextChanged,TextChangedI",
})


-- args is a table, where 1 is the text in Placeholder 1, 2 the text in
-- placeholder 2,...
local function copy(args)
    return args[1]
end

-- Insert `to_insert` here if tabstop `number` is not the empty string
local function insert_if_set(number, to_insert)
    return m(number, ".+", to_insert, "")
end

-- Convenience function for something like `format("$1", $2)` where , is only
-- inserted if `$2` is non-empty
local function format_string_function(trig, fn, pre)
    return s(trig, {
        t(fn),
        t("("),
        t(pre or ""),
        t("\""),
        i(1),
        t("\""),
        -- Insert a comma if we insert arguments
        insert_if_set(2, ", "),
        i(2),
        t(")")
    })
end

ls.snippets = {
    all = {
        format_string_function("fmt", "format!"),
        format_string_function("pln", "println!"),
        format_string_function("wrt", "write!", "w, "),
        s("pfn", {
            t("pub fn "),
            i(1),
            t("("),
            i(2),
            t(")"),
            insert_if_set(3, " -> "),
            i(3),
            t({" {", "\t"}),
            i(4),
            t({"", "}"})
        }),
        s("impl", {
            t("impl"),
            t(" "),
            i(1),
            t({" {", "\t"}),
            i(2),
            t({"", "}"})
        }),
        s("implt", {
            t("impl"),
            insert_if_set(1, " "),
            i(1, "Trait"),
            insert_if_set(1, " for"),
            t(" "),
            i(2),
            t({" {", "\t"}),
            i(3),
            t({"", "}"})
        })
    }
}

--[[
-- Beside defining your own snippets you can also load snippets from "vscode-like" packages
-- that expose snippets in json files, for example <https://github.com/rafamadriz/friendly-snippets>.
-- Mind that this will extend  `ls.snippets` so you need to do it after your own snippets or you
-- will need to extend the table yourself instead of setting a new one.
]]
require("luasnip.loaders.from_vscode").lazy_load()


