# Place me in ~/.config/fish/functions
# Then add me to `fish_vcs_prompt`: `funced fish_vcs_prompt`

function use_color
    perl -pe "s/#yellow\[([^]]*)\]/$(set_color bryellow)\1$(set_color normal)/" \
        # | perl -pe "s/#red\[(.*)\]/$(set_color red)\1$(set_color normal))/" \
        | perl -pe "s/#green\[([^]]*)\]/$(set_color green)\1$(set_color normal)/" \
        | perl -pe "s/#bgred\[([^]]*)\]/$(set_color --background red)\1$(set_color normal))/"
end

function fish_jj_prompt --description 'Write out the jj prompt'
    # Is jj installed?
    if not command -sq jj
        return 1
    end

    # Are we in a jj repo?
    if not jj root --quiet &>/dev/null
        return 1
    end

    # Generate prompt
    jj log --ignore-working-copy --no-graph --color always -r @ -T '
            separate(
                " ",
                coalesce(
                    surround(
                        "#yellow[",
                        "]",
                        if(
                            description.first_line().substr(0, 10).starts_with(description.first_line()),
                            description.first_line().substr(0, 10),
                            description.first_line().substr(0, 10) ++ "…"
                        )
                    ),
                    "#yellow[?]"
                ),
                change_id.shortest(),
                if(conflict, "#bgred[⚔️]"),
                if(empty, "#green[∅]"),
                if(divergent, "(divergent)"),
                if(hidden, "(hidden)"),
            )
    ' | use_color
end
