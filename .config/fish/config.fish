set fish_greeting

function fish_user_key_bindings
  set -g fish_key_bindings fish_vi_key_bindings
  fish_default_key_bindings -M insert
  fish_vi_key_bindings --no-erase insert
end  

if status is-interactive
    # Commands to run in interactive sessions can go here
    bind -M insert -m default jk backward-char force-repaint

    fish_add_path ~/.local/bin
    fish_add_path ~/.cargo/bin
    fish_add_path ~/bin
    fish_add_path ~/bin/oss-cad-suite/bin
    fish_add_path ~/.nix-profile/bin
end

function fish_mode_prompt; end

function fish_mode_prompt_
  switch $fish_bind_mode
    case default
      set_color red
      echo '♦'
    case insert
      set_color green
      echo '♦'
    case replace_one
      set_color blue
      echo '♦'
    case visual
      set_color brmagenta
      echo '♦'
    case '*'
      set_color red
      echo '♦'
  end
  set_color normal
end

set -g PROMPT_START '\e]133;P;k=i\a'
set -g PROMPT_END '\e]133;B\a'

function fish_prompt
    if test -z $SSH_CLIENT
    	#If $SSH_CLIENT is unset
    	set BG_COLOR ""
    else
    	set BG_COLOR (set_color --background cyan)
    end

    string join '' -- (echo -e $PROMPT_START) $BG_COLOR (set_color purple)"$(prompt_pwd --dir-length=5) " (fish_mode_prompt_) $BG_COLOR " "  (set_color red)➔ (set_color normal) " "
end


function fish_right_prompt
    string join '' -- (fish_jj_prompt) " " (echo -e $PROMPT_END)
end

function cf 
    set dir (bfs -type d 2> /dev/null | fzf)

	if ! test -z $dir
    	cd $dir
	end
end

fzf --fish | source

source ~/.config/fish/aliases.fish
jj util completion fish | source

set -Ux HELIX_RUNTIME ~/build/helix/runtime
set -Ux EDITOR /home/frans/.cargo/bin/hx
