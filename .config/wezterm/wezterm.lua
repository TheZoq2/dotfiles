local wezterm = require 'wezterm'

local act = wezterm.action

local scheme = require('scheme').color_scheme
local font_weght = require('scheme').font_weight


local function is_an_editor(name)
   return name:find("vim") or name:find("hx") or name:find("hx")
end


-- https://wezfurlong.org/wezterm/config/lua/pane/get_foreground_process_name.html
-- Equivalent to POSIX basename(3)
-- Given "/foo/bar" returns "bar"
-- Given "c:\\foo\\bar" returns "bar"
-- function basename(s)
--   return string.gsub(s, '(.*[/\\])(.*)', '%2')
-- end

-- wezterm.on('window-focus-changed', function(window, pane)
--     if window:is_focused() then
--         local top_process = basename(pane:get_foreground_process_name())
--         print(top_process)
--         if top_process == 'hx' then
--             local action = wezterm.action.SendString(':rla\r')
--             window:perform_action(action, pane);
--         end
--     end
-- end)

local function non_editor_action(key, mods, action)
    return {
        key = key,
        mods = mods,
        action = wezterm.action_callback(function(window, pane)
            local proc_name = pane:get_foreground_process_name()
            if is_an_editor(proc_name) then
                window:perform_action(act.SendKey{key=key, mods=mods}, pane)
            else
                window:perform_action(action, pane)
            end
        end)
    }
end

return {
    -- NOTE: Do not forget to install terminfo as described in 
    -- https://wezfurlong.org/wezterm/faq.html#how-do-i-enable-undercurl-curly-underlines
    term = "wezterm",
    set_environment_variables = {
    },
    font = wezterm.font_with_fallback {
        {
            family = 'Hasklug Nerd Font',
            weight = font_weght,
            harfbuzz_features = { 'calt=1', 'clig=1', 'liga=1' },
        },
        'Noto Color Emoji',
    },
    font_size=10.0,
    enable_tab_bar=false,
    window_padding = {
        left = 0,
        right = 0,
        top = 0,
        bottom = 0,
    },
    allow_square_glyphs_to_overflow_width="Never",
    color_scheme = scheme,
    color_schemes = {
        ['dark'] = {
            foreground="b2b2b2",
            background="0b151d",
            brights = {
                '373b41',
                'c53e3e',
                'a7e47e',
                'f3d54a',
                '81a2be',
                'b294bb',
                '8abeb7',
                'c5c8c6'
            },
            ansi = {
                '282a2e',
                'af0000',
                '5dc418',
                'de935f',
                '59cbf2',
                '9a1ca4',
                '5e8d87',
                '707880'
            },
            cursor_border = "b2b2b2"
        },
        ['light'] = {
            foreground="4a4543",
            background="ffffff",
            brights = {
                "0b151d",
                "ad251b",
                "339d3a",
                "b69700",
                "003f8e",
                "a6458f",
                "67c6e6",
                "a5a2a2"
            },
            ansi = {
                "5c5855",
                "d03024",
                "01a252",
                "ffa400",
                "01a0e4",
                "a16a94",
                "599fb8",
                "a3a3a3",
            },
            cursor_border = "eeeeee"
        },
    },
    force_reverse_video_cursor = true,

    hyperlink_rules = {
        -- Linkify things that look like URLs and the host has a TLD name.
        -- Compiled-in default. Used if you don't specify any hyperlink_rules.
        {
            regex = '\\b\\w+://[\\w.-]+\\.[a-z]{2,15}\\S*\\b',
            format = '$0',
        },

        -- linkify email addresses
        -- Compiled-in default. Used if you don't specify any hyperlink_rules.
        {
            regex = [[\b\w+@[\w-]+(\.[\w-]+)+\b]],
            format = 'mailto:$0',
        },

        -- file:// URI
        -- Compiled-in default. Used if you don't specify any hyperlink_rules.
        {
            regex = [[\bfile://\S*\b]],
            format = '$0',
        },

        -- Linkify things that look like URLs with numeric addresses as hosts.
        -- E.g. http://127.0.0.1:8000 for a local development server,
        -- or http://192.168.1.1 for the web interface of many routers.
        {
            regex = [[\b\w+://(?:[\d]{1,3}\.){3}[\d]{1,3}\S*\b]],
            format = '$0',
        },
    },
    keys = {
        -- {key = "k", mods = 'CTRL', action = act.SendKey{key="j", mods="CTRL"}},

        non_editor_action("j", "CTRL", act.ScrollByLine(10)),
        non_editor_action("k", "CTRL", act.ScrollByLine(-10)),
        non_editor_action('y', 'CTRL', act.ScrollToPrompt(-1)),
        non_editor_action('h', 'CTRL', act.ScrollToPrompt(1)),
        {key = "i", mods = "CTRL", action = act.SendKey{key = "i", mods='CTRL'}}
    },
    unix_domains = {
        {
            name = "sd",
            proxy_command = {"ssh", "sd-raw", "wezterm", "cli", "proxy"}
        }
    },
    enable_kitty_graphics=true
}

